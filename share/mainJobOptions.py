# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

if __name__ == "__main__":
    #Basic sequence (cfg) and flags 
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    from AthenaConfiguration.AllConfigFlags import initConfigFlags
   
    #This is needed to read AODs
    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

    #Here you can add other modules in addition to this one if you need
    from NIUAthenaExample.NIUConfig import NtupleWriterCfg

    #All of the configuration parameters
    flags = initConfigFlags()
    flags.Input.Files = ["/data/htorre/upgradeSamples/mc21_14TeV/AOD.35454300._000001.pool.root.1"]
    flags.Exec.MaxEvents = 10
    flags.fillFromArgs()
    flags.lock()


    #Run My algorithms, remember to add any algorithm you may need 
    cfg = MainServicesCfg(flags)
    cfg.merge(PoolReadCfg(flags))
    cfg.merge(NtupleWriterCfg(flags,inputClusterContainer="CaloCalTopoClusters"))
    cfg.run()
