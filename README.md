Athena Example for NIU folks
===================================

This is a very barebones example of an algorithm running in athena. It runs over some file given by the user, it recovers EventInfo, a cluster collection and stores some related information in an ntuple.


Step by Step instructions
======================

Setting up the working folder (Athena sparse checkout)
-----------------------

I'm following the instructions from the main [Athena tutorial](https://atlassoftwaredocs.web.cern.ch/gittutorial/). 

First ,you'll need to set up your [fork](https://atlassoftwaredocs.web.cern.ch/gittutorial/gitlab-fork/).

To start a sparse checkout do the following. You only need to do it once, not every time you start working:

```
setupATLAS
lsetup git
git atlas init-workdir https://:@gitlab.cern.ch:8443/atlas/athena.git
```

You now have an athena folder where all of the packages you want to work in should go. Note that once you setup an athena release (below) you have access to all of the packages from that release. You should only checkout the packages that you want to modify! (There is nothing wrong with checking them out, but also, no need). To checkout a package (replace TrigGepPerf by your favorite package):

```
cd athena
git atlas addpkg TrigGepPerf
```

To add a package that is not in athena (this one for example) you should just put them within the athena folder. Anything below that level will get configured/compiled whenever relevant. For example, to get this package

```
cd athena
git clone https://gitlab.cern.ch/htorre/niuathenaexample.git NIUAthenaExample
```

Setting up the environment (1st time)
-------------------------

I recommend starting from scratch (exit you session and login again). From your working folder (wherever you put athena folder in). Replace Athena,main,latest for whatever release you want to work in

```
mkdir build
mkdir run
cd build
setupATLAS
asetup Athena,main,latest
cmake ../athena/Projects/WorkDir
make -j2
source x86_64-centos7-gcc11-opt/setup.sh
```

Setting up the environment (nth time)
-------------------------

Same deal (Of course, if you don't need to rebuild or configure, don't do that. Remember to always source the setup.sh though! If you don't you are not using your local installation)


```
cd build
setupATLAS
asetup restore
cmake ../athena/Projects/WorkDir
make -j2
source x86_64-centos7-gcc11-opt/setup.sh
```

Running
-----------------------------------

From your working folder

```
cd run
cp ../athena/NiuAthenaExample/share/mainJobOptions.py .
```
The mainjobOptions contains a very minimal example of how to add new configurations to the sequence. The idea is the following:

* Every package has modules (inside of the python folder) with configuration functions that add the algorithms and tools of that algorithm to the sequence (Which is now called Component Accumulator).
* You can add as many configurations as you want from the mainJobOptions. I'm adding NTupleWriterCfg from the NIUAthenaExample package (NIUConfig.py file in NIUAthenaExample/python/NIUConfig.py).
* The algorithms from a package are added from those config files to the sequence.
* To run you'll need to point to a file available in your local folder, replace flags.Input.Files with the appropriate one.

Once you are ready to run:

```
python mainJobOptions.py
```

NIUConfig.py itself is also fairly minimal. It has only one function (NtupleWriterCfg) that adds one algorithm (NtupleWriter) to the ComponentAccumulator. There is one configuration property (The name of the input cluster container) that can be modified from the main jobOptions. (With an argument to the configuration fragment). It also initializes the service (histSvc) needed to write ntuples. 

Every module is meant to be ran standalone if required (The module has a main) but I didn't configure it properly for this example as we are calling NtupleWriterCfg from an external jobOptions. You can safely ignore that part.


The algorithm is defined in NIUAthenaExample/src/NtupleWriter.cxx nd NIUAthenaExample/src/NtupleWriter.h. The main complication is the way to define StoreGate handles (In the header). These are 'reading' type handles. You may need other types depending on what you are doing. In this case I'm reading the cluster container and the eventinfo in order to get some info from it and dumping it into an ntuple. 

Authors
---------------------------
* **Hector de la Torre**


