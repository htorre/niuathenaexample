# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator


def NtupleWriterCfg(flags,inputClusterContainer="CaloCalTopoCluster"):
    result = ComponentAccumulator()

    alg = CompFactory.NtupleWriter("NtupleWriter",inputClusterContainer=inputClusterContainer)

    # add the algorithm to the result
    result.addEventAlgo(alg)

    #We need The hist service to output nutples 
    histSvc = CompFactory.THistSvc(Output=["outputStream DATAFILE='outputNtuple.root' OPT='RECREATE'"])
    result.addService(histSvc)

    return result


if __name__ == "__main__":
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    from AthenaConfiguration.AllConfigFlags import initConfigFlags
    flags = initConfigFlags()
    flags.Exec.MaxEvents = 10
    flags.fillFromArgs()
    flags.lock()

    cfg = MainServicesCfg(flags)
    cfg.merge(NtupleWriterCfg(flags))
    cfg.run()
