/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#ifndef NIUATHENAEXAMPLE_NTUPLEWRITER_H
#define NIUATHENAEXAMPLE_NTUPLEWRITER_H

#include "AthenaBaseComps/AthAlgorithm.h"
#include "Gaudi/Property.h"
#include "GaudiKernel/ToolHandle.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "TTree.h"
#include "GaudiKernel/ITHistSvc.h"


#include <map>
#include <string>
#include <utility>
#include <vector>

class NtupleWriter : public AthAlgorithm {
 public:
  NtupleWriter(const std::string &name, ISvcLocator *pSvcLocator);

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

 private:
  //Service required for output of trees and histograms
  ServiceHandle<ITHistSvc> m_histSvc{this, "THistSvc", "THistSvc"};

  //Handles to define collections to recover, these things are now configurable automatically !
  SG::ReadHandleKey<xAOD::EventInfo> m_eventInfo {this, "eventInfo", "EventInfo", "key to read in an EventInfo object"};
  SG::ReadHandleKey<xAOD::CaloClusterContainer> m_inputClusterContainer { this, "inputClusterContainer", "CaloCalTopoClusters", "Key to 420 cluster collection"};
  

  //Tree definition (Add your branches here)
  TTree * m_outputTree;
  int m_eventNumber;
  int m_runNumber;
  std::vector<double> m_clusterEt;
  std::vector<double> m_clusterEta;
  std::vector<double> m_clusterPhi;

};

#endif
