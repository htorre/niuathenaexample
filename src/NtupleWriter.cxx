/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
 
*/

/*
 * This algorithm is an example on how to open some basic Collections in Athena and write the output into a ntuple
 */

#include "NtupleWriter.h"

NtupleWriter::NtupleWriter(const std::string &name, ISvcLocator *pSvcLocator)
    : AthAlgorithm(name, pSvcLocator){

}

StatusCode NtupleWriter::initialize() {
  ATH_MSG_INFO("Initializing " << name() << "...");

  //Handles need to be initialized!
  CHECK(m_eventInfo.initialize());
  CHECK(m_inputClusterContainer.initialize());


  //Define the tree
  m_outputTree = new TTree("myTree","myTree");
  //Define the branches
  m_outputTree->Branch("clusterEt", &m_clusterEt);
  m_outputTree->Branch("clusterEta", &m_clusterEta);
  m_outputTree->Branch("clusterPhi", &m_clusterPhi);
  m_outputTree->Branch("eventNumber", &m_eventNumber);
  m_outputTree->Branch("runNumber", &m_runNumber);
  //Register it with the appropriate service 
  CHECK(m_histSvc->regTree("/outputStream/myTree",m_outputTree));

  return StatusCode::SUCCESS;
}

StatusCode NtupleWriter::execute() {
  ATH_MSG_INFO("Executing " << name() << "...");

  //Recovering event Info
  auto eventInfo = SG::makeHandle(m_eventInfo);
  CHECK(eventInfo.isValid());
  m_eventNumber = eventInfo->eventNumber();
  m_runNumber = eventInfo->runNumber();

  //clering vectors
  m_clusterEt.clear();
  m_clusterEta.clear();
  m_clusterPhi.clear();

  //Recovering cluster container
  auto inputClusterContainer = SG::makeHandle(m_inputClusterContainer);
  CHECK(inputClusterContainer.isValid());
  for(auto iCluster : *inputClusterContainer){
    m_clusterEt.push_back(iCluster->et());
    m_clusterEta.push_back(iCluster->eta());
    m_clusterPhi.push_back(iCluster->phi());
  }
  
 //Don't forget to fill your tree 
  m_outputTree->Fill();


  return StatusCode::SUCCESS;
}

StatusCode NtupleWriter::finalize() {
  
  ATH_MSG_INFO("Finalizing " << name() << "...");

  return StatusCode::SUCCESS;
}
